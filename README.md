# Openmanage Enterprise python scripts
Set of useful python scripts to perform different actions to OME using its REST API. 

## Important
Please, before execute any of them, make sure you have installed the prerequisites:
~~~
pip install -r requirements.txt
~~~

Miguel Sama (Miguel_Sama@Dell.com)

