#Imports
import yaml
import requests
import os
import time
from collections import OrderedDict
import markdown
#Disable warnings
requests.packages.urllib3.disable_warnings() 

#Constants
CONFIG_FILENAME = "config.yml"
OUTPUT_FILE = "hardware_report.html"

def readConfigFile(filename):
    with open(os.getcwd() + "/" + filename) as f:
        try:
            data = yaml.safe_load(f)
            return data
        except yaml.YAMLError as exc:
            print(exc)

def getServerAuthenticationToken(server_addr, ome_user, ome_pass):
    #Implement error handling
    response = requests.post('https://' + server_addr + '/api/SessionService/Sessions', 
                                headers={'Content-Type': 'application/json'}, 
                                json={"UserName":ome_user, "Password":ome_pass, "SessionType":"API"}, 
                                verify=False)
    #Raise an exception in case of invalid credentials
    response.raise_for_status()
    token = response.headers['x-auth-token']
    sessionId = response.json()["Id"]
    return token, sessionId

def closeSessionToken(server_addr, auth_token, sessionId):
    response = requests.delete('https://' + server_addr + "/api/SessionService/Sessions('" + sessionId + "')",
                                headers={'Content-Type': 'application/json', 'x-auth-token':auth_token},
                                verify=False)
    print("Session closed")

def getDeviceList(server_addr, auth_token):
    response = requests.get('https://' + server_addr + '/api/DeviceService/Devices',
                            headers={'Content-Type': 'application/json', 'x-auth-token':auth_token},
                            verify=False)
    devices = response.json()
    deviceList = {}
    #Retrieve from OME all devices IDs
    for device in devices["value"]:
        #I might need to filter by Type 1000 (which I think it means baremetal server)
        deviceList[device["Id"]] = {"model":device["Model"], "serviceTag":device["DeviceServiceTag"]}
    return deviceList

def getProcessorDetails(inventoryInfo):
    processorDetails = []
    for processor in inventoryInfo:
        d = OrderedDict()
        d["SlotNumber"] = processor["SlotNumber"]
        d["ModelName"] = processor["ModelName"]
        d["NumberOfCores"] = processor["NumberOfCores"]
        d["MaxSpeed"] = processor["MaxSpeed"]
        d["CurrentSpeed"] = processor["CurrentSpeed"]
        d["Family"] = processor["Family"]
        processorDetails.append(d)
    return processorDetails

def getMemoryDetails(inventoryInfo):
    memoryDetails = []
    for dimm in inventoryInfo:
        d = OrderedDict()
        d["Name"] = dimm["Name"]
        d["BankName"] = dimm["BankName"]
        d["Size"] = dimm["Size"]
        d["Manufacturer"] = dimm["Manufacturer"]
        d["PartNumber"] = dimm["PartNumber"]
        d["SerialNumber"] = dimm["SerialNumber"]
        d["TypeDetails"] = dimm["TypeDetails"]
        d["Speed"] = dimm["Speed"]
        d["CurrentOperatingSpeed"] = dimm["CurrentOperatingSpeed"]
        memoryDetails.append(d)
    return sorted(memoryDetails, key = lambda i: i["BankName"])

def getServerDetails(server_addr, auth_token, serverID):
    serverDetails = {}
    response = requests.get('https://' + server_addr + '/api/DeviceService/Devices(' + str(serverID) + ')/InventoryDetails',
                            headers={'Content-Type': 'application/json', 'x-auth-token':auth_token},
                            verify=False)
    hardwareInfo = response.json()["value"]
    for hardwarePart in hardwareInfo:
        if hardwarePart["InventoryType"] == "serverProcessors":
            serverDetails["serverProcessors"] = getProcessorDetails(hardwarePart["InventoryInfo"])
        elif hardwarePart["InventoryType"] == "serverMemoryDevices":
            serverDetails["serverMemoryDevices"] = getMemoryDetails(hardwarePart["InventoryInfo"])
    return serverDetails

def writeTable(itemList):
    setHeaders = False #var used to set headers for the first time
    table = ""
    for item in itemList:
        if not setHeaders:
            for key,value in item.items():
                table += "|" + key
            table += "|\n  "
            for i in range(len(item)):
                table += "|---"
            table += "|\n  "
            setHeaders = True
        for key,value in item.items():
            table += "|" + str(value)
        table += "|\n"
    table += "\n"
    return table

def fromDictToMarkdown(hwReport):
    markdownReport = ""
    #Name of the report
    markdownReport += "# Hardware report - " + time.strftime("%d/%m/%Y") + "\n"
    #Write each machine
    for serviceTag in hwReport:
        #Write title of the report
        markdownReport += "## " + hwReport[serviceTag]["ServerModel"] + " - " + serviceTag + "\n"
        #Write CPU details
        markdownReport += "### CPU details\n"
        markdownReport += writeTable(hwReport[serviceTag]["serverProcessors"])
        #Write Memory details
        markdownReport += "### Memory details\n"
        markdownReport += writeTable(hwReport[serviceTag]["serverMemoryDevices"])
    return markdownReport

def writeReportToFile(markdownReport):
    html = markdown.markdown(markdownReport, extensions=['extra'])
    with open(os.getcwd() + "/" + OUTPUT_FILE, "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
        output_file.write(html)


#Main program
if __name__ == "__main__":
    data = readConfigFile(CONFIG_FILENAME)
    hardwareReport = {}
    try:
        token, session_id = getServerAuthenticationToken(data["ome_address"], data["ome_username"], data["ome_password"])
    except requests.exceptions.HTTPError:
        print("Error, wrong OME credentials. Please verify and try again.")
        exit(1)
    deviceList = getDeviceList(data["ome_address"], token)
    for deviceID in deviceList:
        hardwareReport[deviceList[deviceID]["serviceTag"]] = getServerDetails(data["ome_address"], token, deviceID)
        hardwareReport[deviceList[deviceID]["serviceTag"]]["ServerModel"] = deviceList[deviceID]["model"]
        print("[INFO] Retrieved hardware info from " + deviceList[deviceID]["model"] + " with service tag " + deviceList[deviceID]["serviceTag"])
    #Close session
    closeSessionToken(data["ome_address"], token, session_id)
    #Create report
    finalReport = fromDictToMarkdown(hardwareReport)
    print("[INFO] Writing report")
    writeReportToFile(finalReport)
    
    