#Imports
import yaml
import requests
import os
#Disable warnings
requests.packages.urllib3.disable_warnings() 

#Constants
CONFIG_FILENAME = "config.yml"

def readConfigFile(filename):
    with open(os.getcwd() + "/" + filename) as f:
        try:
            data = yaml.safe_load(f)
            return data
        except yaml.YAMLError as exc:
            print(exc)

def getServerAuthenticationToken(server_addr, ome_user, ome_pass):
    #Implement error handling
    response = requests.post('https://' + server_addr + '/api/SessionService/Sessions', 
                                headers={'Content-Type': 'application/json'}, 
                                json={"UserName":ome_user, "Password":ome_pass, "SessionType":"API"}, 
                                verify=False)
    #Raise an exception in case of invalid credentials
    response.raise_for_status()
    token = response.headers['x-auth-token']
    sessionId = response.json()["Id"]
    return token, sessionId


def closeSessionToken(server_addr, auth_token, sessionId):
    response = requests.delete('https://' + server_addr + "/api/SessionService/Sessions('" + sessionId + "')",
                                headers={'Content-Type': 'application/json', 'x-auth-token':auth_token},
                                verify=False)
    print("Session closed")

def getAllUsers(server_addr, auth_token):
    response = requests.get('https://' + server_addr + '/api/AccountService/Accounts',
                            headers={'Content-Type': 'application/json','x-auth-token':auth_token}, 
                            verify=False)
    return response.json()

def modifyUser(server_addr, auth_token, username, newPassword, userId):
    response = requests.put('https://' + server_addr + "/api/AccountService/Accounts('" + userId + "')",
                            headers={'Content-Type': 'application/json','x-auth-token':auth_token}, 
                            json={"Id":userId, "Name":username, "UserName":username, "Password":newPassword, "Enabled":True},
                            verify=False)
    print("User [" + username + "] modified")

def getUserId(username, userlist):
    for user in userlist["value"]:
        if user["UserName"] == username:
            return user["Id"]
    return -1

#Execute the main function
if __name__ == "__main__":
    data = readConfigFile(CONFIG_FILENAME)
    try:
        token, session_id = getServerAuthenticationToken(data["ome_address"], data["ome_username"], data["ome_password"])
    except requests.exceptions.HTTPError:
        print("Error, wrong OME credentials. Please verify and try again.")
        exit(1)
    users = getAllUsers(data["ome_address"], token)
    for user in data["users"]:
        #Check if the user is in the system by retreiving its userId
        usedId = getUserId(user["username"], users)
        if usedId == -1:
            print("User [" + user["username"] + "] doesn't exist in OME and will not be changed")
        else:
            modifyUser(data["ome_address"], token, user["username"], user["new_password"], usedId)

    #Close OME http session token
    closeSessionToken(data["ome_address"], token, session_id)




