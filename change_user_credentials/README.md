# Change user credentials script for OME
Simple script to change user credentials on OME.  
**IMPORTANT**: At the moment it just allows you to change *admin* credentials.  

## Usage
First of all, please configure the file *config.yml* accordingly:  
Example of config.yml
~~~
ome_address: 10.0.0.1
ome_username: admin
ome_password: admin

users:
  - username: admin
    new_password: admin2
~~~
**ome_address**: Ip address of Openmanage Enterprise
**ome_username**: Admin username with enough privileges to change passwords from users
**ome_password**: Password from the admin user above.

**users**: List of users to modify. Each element has two keys (*username* and *new_password*) which are the username to modify and the new password to set respectively.  
  
Miguel Sama (miguelsamamerino@dell.com)

